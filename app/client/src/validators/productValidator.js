import store from '@/store'

export function validateForm()
{
    const validator = validate()
    
    let isValid = Object.keys(validator).every((k) => {
        return validator[k].every(boolean => boolean)
    })

    return isValid
}

export function validateSku(value)
{
    return store.getters.products.find(product => product.sku == value) ? false : true
}

export function notEmptyField(value)
{
    return value == '' ? false : true
}

export function validateNumber(value)
{
    return parseFloat(value) < 0 ? false : true
}

function validate()
{
    return {
        sku: [validateSku(store.getters.newProduct.sku), notEmptyField(store.getters.newProduct.sku)],
        name: [notEmptyField(store.getters.newProduct.name)],
        price: [validateNumber(store.getters.newProduct.price), notEmptyField(store.getters.newProduct.price)],
        attribute: [attributeValidator(store.getters.newProduct.attribute)],
    }
}

function attributeValidator(value)
{
    const numbers = value.split("x")
    return numbers.every(number => validateNumber(number) && notEmptyField(number))
}