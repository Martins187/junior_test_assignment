import { notEmptyField, validateSku, validateNumber } from '@/validators/productValidator'

export function numberWarning(value, attribute)
{
    return validateNumber(value) ? '' : attribute + 'must be > 0!'
}

export function skuWarning(value)
{
    return validateSku(value) ? '' : 'Sku already taken!'
}

export function emptyFieldWarning(value)
{
    return notEmptyField(value) ? '' : '*Required'
}