import axios from 'axios'

class Products
{
    static serverPath = 'http://localhost:31243'

    static get()
    {
        return axios.get(`${this.serverPath}/products`).catch(error =>{
            console.log(error)
        })
    }

    static add(data)
    {
        return axios.post(`${this.serverPath}/products`, { data: data}).catch(error =>{
            console.log(error)
        })
    }

    static delete(data)
    {
        return axios.delete(`${this.serverPath}/products`, { data: data}).catch(error =>{
            console.log(error)
        })
    }
}

export default Products