import { validateForm } from '@/validators/productValidator'
import Products from './api/Products'
import { createStore } from 'vuex'

const state = {
    products:[],
    newProduct: getProductTemplate(),
    isValidForm: false,
}

function getProductTemplate()
{
    return {
        sku: '',
        name: '',
        price: '',
        type: '',
        attribute: '',
        units: '',
    }
}

const mutations = {

    assignProducts(state, products) 
    {
        state.products = [...products]
    },

    changeValue(state, changes) 
    {
        state.newProduct[changes.parameter] = changes.value
    },

    addProduct(state, product) 
    {
        state.products.push(product)
    },

    setIsValidForm(state, value) 
    {
        state.isValidForm = value
    },

    resetNewProduct(state) 
    {
        state.newProduct = getProductTemplate()
    },

    selectProduct(state, object) 
    {
        let index = state.products.findIndex( product => product.sku == object.sku)
        state.products[index].isSelected = !state.products[index].isSelected
    },

    massDelete(state) 
    {
        state.products = state.products.filter( product => !product.isSelected)
    },
}

const actions = {

    fetchData({ commit }) 
    {
        Products.get().then((response)=>
        {
            if(response){
                commit('assignProducts', response.data)
            }
        })
        
    },

    addProduct({ dispatch, getters, commit }) 
    {
        dispatch("validateForm")
        
        if(getters.isValidForm) 
        {
            let newProduct = getters.newProduct

            Products.add(newProduct).then((response)=>
            {
                if(response)
                {
                    commit('addProduct', newProduct)
                }

            })
        }
    },

    massDelete({ commit, getters }) 
    {
        let selectedProducts = []

        for(let product in getters.selectedProducts)
        {
            if(getters.selectedProducts[product].checked)
            {
                selectedProducts.push(getters.selectedProducts[product].name)
            }
        }
        
        Products.delete(selectedProducts).then((response) => 
        {
            if(response)
            {
                commit('massDelete')
            }

        })
    },

    changeValue({ commit }, changes) 
    {
        commit("changeValue", changes)
    },

    validateForm({ commit }) 
    {
        commit('setIsValidForm', validateForm())
    },

    resetNewProduct({ commit }) 
    {
        commit('resetNewProduct')
    },

    selectProduct({ commit }, product) 
    {
        commit('selectProduct', product)
    },
}

const getters = {

    products(state) 
    {
        return state.products
    },

    newProduct(state) 
    {
        return state.newProduct
    },

    isValidForm(state) 
    {
        return state.isValidForm
    },

    selectedProducts()
    {
        return document.getElementsByClassName('delete-checkbox');
    }
}

export default createStore ({
    state,
    mutations,
    actions,
    getters
})