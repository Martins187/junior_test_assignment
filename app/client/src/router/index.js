import { createWebHistory, createRouter } from "vue-router"
import Index from '../views/Index.vue'
import AddProduct from '../views/AddProduct.vue'

const routes = [
    {
        path: '/',
        component: Index,
        name: 'Product list',
    },
    {
        path: '/add-product',
        component: AddProduct,
        name: 'Add product',
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router