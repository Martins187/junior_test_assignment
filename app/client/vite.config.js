import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
    resolve:{
        alias:{
            '@' : require('path').resolve(__dirname, 'src')
        },
    },
    
    plugins: [vue()],
})
