<?php

/**
 * This file is executed only once when setting up the project,
 * it creates the necessary table in the database.
 */

include 'config.php';

$dbh = new PDO('mysql:dbname='.DB_NAME.';host='.DB_HOST, DB_USER, DB_PASS);

$statements = [
    'CREATE TABLE IF NOT EXISTS`products` (
    `id` int(11) NOT NULL,
    `sku` varchar(20) NOT NULL,
    `name` varchar(30) NOT NULL,
    `price` float NOT NULL,
    `type` enum("Dvd","Book","Furniture","") NOT NULL,
    `attribute` varchar(30) NOT NULL,
    `units` enum("mb","kg","m","") NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;',

    'ALTER TABLE `products`
    ADD PRIMARY KEY (`id`);',

    'ALTER TABLE `products`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
    COMMIT;'
];

foreach($statements as $statement)
{
    $dbh->exec($statement);
}
