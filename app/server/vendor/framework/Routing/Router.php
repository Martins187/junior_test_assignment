<?php

namespace Framework\Routing;

use Framework\Request;
use Framework\Response\Response;
use App\Controllers;

class Router {
	
	protected $routes = [];
	
	/**
	 * Defining the routes.
	 */
	function __construct(array $routes)
	{
		$this->routes = $routes;
	}
		
	/**
	 * Comparing all the routes from the 'routes'
	 * to the requested one. If there is a match,
	 * the corresponding action will be initiated.
	 */
	function match(Request $request): Response
	{
		foreach($this->routes as $route)
		{
			if($route->path == $request->path && $route->httpMethod == $request->httpMethod)
			{
				$method = $route->method;
				return $route->action::$method();
			}
		}
	}
}
	