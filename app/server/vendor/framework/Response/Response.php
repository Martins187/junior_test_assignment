<?php

namespace Framework\Response;

class Response 
{
    public  $content;
    
    /**
     * All the data sent back to the client will
     * be in the json format.
     */
    function __construct($data)
    {
        $this->content = $this->toJson($data);
    }

    /**
     * This header is supposed to be added when the 
     * data in the http request is invalid.
     */
    public function sendServerError()
    {
        header('X-PHP-Response-Code: 400', true, 400);
    }

    /**
     * Converts data passed as a parameter into 
     * json format.
     */
    public function toJson($data) : string
    {
        return json_encode($data);
    }

    /**
     * Defining the type of the data returned.
     */
    public function sendHeaders()
    {
        header('Content-Type: application/json; charset=utf-8');
    }

    /**
     * Echoing the actual response.
     */
    public function sendContent()
    {
        echo $this->content;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
    }
}