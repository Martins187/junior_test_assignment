<?php

namespace Framework;

use Framework\Validate\Validator;

class Request
{
    public $requestData;
    public $path;
    public $httpMethod;

    /**
     * Defining all the properties of the request.
     */
    public function __construct()
    {
        $this->requestData  = $this->getRequest();
        $this->path     = $this->getPath();
        $this->httpMethod   = $this->getHttpMethod();
    }

    /**
     * Decoding the data from the request.
     */
    public function getRequest()
    {
        return json_decode(file_get_contents('php://input'), true);
    }

    /**
     * Defining the requested path.
     */
    public function getPath() : string
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Defining the http request method.
     */
    public function getHttpMethod() : string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function validateObject() : array
    {
        return (new Validator)->validateObject($this->requestData['data'], $this->rules());
    }

    public function validateArray() : array
    {
        return (new Validator)->validateArray($this->requestData, $this->rules());
    }

    public static function create() : self
    {
        return new static();
    }
}


