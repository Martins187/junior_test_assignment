<?php

namespace Framework\Validate;

use App\Models\Product;

/**
 * Each function should aquire the rules from the 
 * relavant validator and return the original 
 * request if all the rules are true.
 */
class Validator extends RuleHandler
{
    public function validateObject(array $request, array $rules)
    {
        foreach($rules as $key => $values)
        {
            $rules = explode("|", $values);

            foreach($rules as $rule)
            {
                $rulesParameters = explode(":", $rule);
                $method = $rulesParameters[0];

                if(!$this->$method($rulesParameters[1] ?? null, $request[$key]))
                {
                    response()->sendServerError();
                    return;
                }
            }
        }

        return $request;
    }

    public function validateArray(array $request, string $method)
    {
        foreach($request as $item)
        {
            if(!$this->$method('', $item))
            {
                response()->sendServerError();
                return;
            }
        }

        return $request;
    }
}