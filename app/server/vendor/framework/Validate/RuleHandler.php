<?php

namespace Framework\Validate;

/**
 * Defining all the related validation rules.
 */
class RuleHandler
{
    public function unique($ruleParameter, $value) : bool
    {
        $ruleValues = explode("-", $ruleParameter);
        $model = 'App\Models\\'.$ruleValues[0];
        $allRows = $model::all($ruleValues[2], $ruleValues[1]);

        $parameterValueArray = array_column(json_decode($allRows->content), $ruleValues[2]);
        
        return !array_search($value,  $parameterValueArray);
    }

    public function required($ruleParameter, $value) : bool
    {
        return $value != '' && preg_match('/^[A-Za-z0-9_.,\s-]*$/', $value);
    }

    public function regex($ruleParameter, $value) : bool
    {
        return !preg_match($ruleParameter, $value);
    }

    public function in($ruleParameter, $value) : bool
    {
        $ruleValues = str_replace (",", "|", $ruleParameter);
        return preg_match('~\b('.$ruleValues.')\b~i', $value) ? true : false;
    }

    public function number($ruleParameter, $value) : bool
    {
        return is_numeric($value);
    }

    public function attribute($ruleParameter, $value) : bool
    {
        foreach(explode('x', $value) as $number)
        {
            if(!$this->number(null, $number))
            {
                return false;
            }
        }

        return true;
    }
}