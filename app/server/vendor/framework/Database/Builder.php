<?php

namespace Framework\Database;

use Framework\Response\Response;

use PDO;

class Builder {
	
	protected $dbh;
    protected $query = '';

    /**
     * This property is used only when the functions 
     * in the querybuilder are called that are 
     * supposed to add clauses
     */
    protected $table = '';

    /**
     * Defining database handler as a new instance 
     * of the 'Connection'.
     */
    function __construct()
    {
        $this->dbh = (new Connection)->connect();
    }

    /**
     * This function is for adding a sql statement
     * (add, delete) and not defining rules, 
     * so it prepends the statement.
     */
    public function prependStatement(string $sql)
    {
        $this->query = $sql . $this->query;
    }

    /**
     * Appending a new sql statement or clause to 
     * already existing query or creating a new one.
     */
    public function addStatement(string $sql)
    {
        $this->query .= $sql;
    }

    /**
     * Preparing already built query and executing
     * it with the initial connection to the db.
     */
    public function execute()
    {
        $this->query = $this->dbh->prepare($this->query);
        $this->query->execute();
    }

    /**
     * Executing the query and fetching all the data
     * from the db.
     */
    public function fetchAll() : Response
    {
        $this->execute();
        return response($this->query->fetchAll(PDO::FETCH_OBJ));
    }
}

