<?php

namespace Framework\Database;

use Framework\Response\Response;

class QueryBuilder extends Builder
{
    /**
     * Creates a new row in the db. 
     */
    public function create(array $data, string $table) : Response
    {   
        foreach($data as $key => $value)
        {
            $indexes[] = $key;
            $values[] = $value;
        }

        $indexes = join(",", $indexes);
        $values = join("','", $values);
     
        $this->prependStatement("INSERT INTO $table ($indexes) VALUES('$values')");
        $this->execute();

        return response('success');
    }

    /**
     * This function is for adding a clause and defining 
     * to which items will the statement apply. 
     */
    public function whereIn(array $data, string $table) : self
    {
        $selected = join("','", $data['selected']);
        $this->addStatement("WHERE ". $data['collumn'] ." IN ('$selected')");
        $this->table = $table;

        return $this;
    }

    /**
     * Adding a delete statement to the query and executing it.
     */
    public function delete() : Response
    {
        $this->prependStatement('DELETE FROM '.$this->table.' ');
        $this->execute();

        return response('success');
    }

    /**
     * Fetching all the data from the particular table.
     */
    public function all(string $collumn, string $table) : Response
    {
        $this->prependStatement('SELECT '.$collumn.' FROM '.$table.' ');

        return $this->fetchAll();
    }
}