<?php

namespace App\Routing;

use App\Controllers\ProductController;
use Framework\Routing\Route;

class Routes 
{
	function register() : Route
	{
		$routes = new Route;
		$routes->get('/products', ProductController::class, 'index'); 
 		$routes->post('/products', ProductController::class, 'store');		
		$routes->delete('/products', ProductController::class, 'destroy');
		
		return $routes;
	}
	
}