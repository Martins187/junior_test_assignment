<?php

namespace App\Requests;

use Framework\Request;

class ProductStoreRequest extends Request
{
    /** 
     * Validation rules for each property of the object (product 
     * in this case) are separated with "|" sign.
     */

    public function rules(): array
    {
        return [
            'sku'       => 'required|unique:Product-products-sku',
            'name'      => 'required|regex:/a-Z-0-9-\s/',
            'type'      => 'required|in:dvd,book,furniture',
            'price'     => 'required|number',
            'attribute' => 'required|attribute',
        ];
    }
}