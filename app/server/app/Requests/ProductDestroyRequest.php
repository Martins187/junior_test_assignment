<?php

namespace App\Requests;

use Framework\Request;

class ProductDestroyRequest extends Request
{
    /** 
     * As destroy request will recieve an array,
     * each item must meet general validation 
     * terms of any field.
     */

    public function rules() : string
    {
        return 'required';
    }
}
