<?php

namespace App\Models;

use Framework\Database\Model;

/**
 * Only the tableName attribute is for the use,
 * the rest are for the convinience.
 */
class Product extends Model
{
	public $sku;
	public $name;
	public $type;
	public $price;
	public $attribute;
	public $tableName = 'products';
}
