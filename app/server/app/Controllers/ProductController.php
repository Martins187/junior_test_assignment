<?php

namespace App\Controllers;

use stdClass;
use App\Models\Product;
use Framework\Response\Response;
use App\Requests\ProductStoreRequest;
use App\Requests\ProductDestroyRequest;

class ProductController
{	
	/**
	 * Fetching all the rows from the 'products' table.
	 */
	public function index(): Response
	{
		return Product::all('*');
	}

	/**
	 * Returns 'success' if all product properties
	 * were valid and the object was successfully 
	 * added to the db.
	 */
	public function store(): Response
	{
		$validated = (new ProductStoreRequest)->validateObject();

		return Product::create($validated);
	}
	
	/**
	 * Returns 'success' if the requested products'
	 * ids were valid and each relavant row was 
	 * deleted successfully.
	 */
	public function destroy(): Response
	{
		$validated = (new ProductDestroyRequest)->validateArray();

		return Product::whereIn([
			'collumn' => 'sku', 
			'selected' => $validated
		])->delete();
	}
}
	