Scandiweb Junior Developer Test Assignment
---
---
### Setup

1. Make sure you have docker installed in your local environment. 
2. Clone the repository and go to the root diectory of the project. 
3. Run command "docker-compose up".
4. The app will be running at http://localhost:8080

### Demo
http://46.101.229.217:8080/
